from typing import Tuple
import findspark
import pandas as pd
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import when

from pyspark.sql import functions as F
from pyspark.sql.window import Window
from pyspark.ml.feature import VectorAssembler, Normalizer
from pyspark.ml import Pipeline
from pyspark.ml.regression import GBTRegressor

from pyspark.sql.dataframe import DataFrame

import matplotlib.pyplot as plt
import pandas as pd


class Session():
    def __init__(self) -> None:
        findspark.init()
        self.sc = SparkContext(conf=SparkConf())
        self.sc.setLogLevel("ERROR")
        self.spark = SparkSession(sparkContext=self.sc)
        self.df = self.__create_df()
        self.df2 = self.__create_df2()
        self.best_scorer = None
        self.TPA_per_Season = None

    def __create_df(self) -> DataFrame:
        df = self.spark.read.csv("seasons_stats.csv", header=True, inferSchema=True)
        df = df.select(["Year", "Player", "PTS", "3PA"])
        df = df.filter(df.Year >= 1980)
        return df
    
    def __create_df2(self) -> DataFrame:
        df2 = self.spark.read.csv("seasons_wiki.csv", header=True, inferSchema=True)
        df2 = df2.selectExpr("`Year[c]` as Year", "`No. of teams[d]` as Teams", "`No. of games[e]` as TeamGames")
        df2 = df2.na.drop().filter(df2.Year >= 1980)
        df2 = df2.withColumn("Games", df2.Teams * df2.TeamGames / 2)
        df2 = df2.withColumn("Games", when(df2.Year == 2020, 1059).when(df2.Year == 2013, (82*30)/2-1).otherwise(df2.Games))
        return df2
    
    def get_best_scorer(self) -> DataFrame:
        df = self.df.groupBy("Player").mean().sort("avg(PTS)", ascending=False)
        return df.select(["Player", "avg(PTS)"])
    
    def get_3PA_per_Season(self) -> DataFrame:
        # table with 3PA sum per season
        sum3PA = self.df.groupBy("Year").sum("3PA")
        # table with number of games per season
        sumGames = self.df2.select("Year", "Games")
        # merging
        df = sum3PA.join(sumGames, sum3PA.Year == sumGames.Year).select(sum3PA.Year, "sum(3PA)", "Games")
        df = df.withColumn("3PA/G", df["sum(3PA)"] / df.Games).select("Year", "3PA/G").orderBy("Year")
        self.TPA_per_Season = df
        return df
    
    def predict_future(self) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        if not isinstance(self.TPA_per_Season, DataFrame):
            self.get_3PA_per_Season()
        
        df = self.__preprocess_data()
        training, test = self.__train_test_split(df)
        
        training, test = self.__predict(training, test)
        
        predictions = self.__prepare_predictions_table(test)
        return training.toPandas(), test.toPandas(), predictions  # type: ignore

    def __preprocess_data(self):
        my_window = Window.orderBy("Year")
        
        df: DataFrame = self.TPA_per_Season  # type: ignore
        df = df.withColumn("diff", df["3PA/G"] - F.lag("3PA/G", 1).over(my_window))
        
        for i in range(1, 6):
            df = df.withColumn(f"diff_hist_{i}", F.lag("diff", i).over(my_window))
            df = df.withColumn(f"label_{i}", df["diff"] - F.lag("diff", -i).over(my_window))
        
        df = df.where(df.Year > 1985)  # get rid of none in features
        
        vectorAssembler_stage = VectorAssembler(inputCols=["diff", *[f"diff_hist_{i}" for i in range(1, 6)]], outputCol="features_before")
        normalizer = Normalizer(inputCol="features_before", outputCol="features", p=1.0)
        pipeline = Pipeline(stages=[vectorAssembler_stage, normalizer])
        pipeline_model = pipeline.fit(df)
        
        df = pipeline_model.transform(df).select("Year", "features", *[f"label_{i}" for i in range(1, 6)], "3PA/G")
        return df

    def __train_test_split(self, df):
        training = df.where(df.Year < 2010)
        test = df.where(df.Year >= 2010)
        return training,test

    def __predict(self, training, test):
        for i in range(1, 6):
            lr = GBTRegressor(featuresCol = 'features', labelCol = f'label_{i}')
            lr_model = lr.fit(training)
            pred_test = lr_model.transform(test)
            pred_training = lr_model.transform(training)
            training = training.join(pred_training.select("Year", "prediction"), on="Year").withColumnRenamed("prediction", f"prediction_{i}")
            test = test.join(pred_test.select("Year", "prediction"), on="Year").withColumnRenamed("prediction", f"prediction_{i}")
            print(f"\rDONE: {i}/5", end="")
        print(" " * 10)
        return training, test

    def __prepare_predictions_table(self, test):
        predictions = test.where(test["Year"] == 2021).select("3PA/G", *[f"prediction_{i}" for i in range(1, 6)]).toPandas().transpose()
        predictions.index =  [year for year in range(2021, 2027)]  # type: ignore
        predictions["3PA/G"] = predictions[0][2021] + predictions[0]
        predictions["3PA/G"][2021] = predictions[0][2021]
        predictions = predictions.drop(0, axis=1)
        return predictions

    def plot_predicitons(self, training, test, labels):
        colors = [(0.15*i, 0.15*i, 1) for i in range(5)]
        training = training.set_index("Year")
        test = test.set_index("Year")
        to_plot = pd.concat([training, test])
        to_plot.plot(color=colors, y=[f"label_{i}" for i in labels], figsize=(20,8))
        to_plot.plot(color=colors, y=[f"prediction_{i}" for i in labels], ax=plt.gca(), style="--")
    
    def close(self):
        self.sc.stop()
        del self