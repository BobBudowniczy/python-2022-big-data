from sqlalchemy import create_engine
from os import system, popen, chdir
from time import sleep
from common import *
from queries import *

DATABASE = "requests"


@logging_decoratorA
def _test3(df = None):
    """Test the most common aspects of the forecast using mysql server.

    Args:
        df ([pd.DataFrame], optional): [dataframe to work on]. Defaults to None.
    """
    if not isinstance(df, pd.DataFrame): df = load_data_csv()
    with SQLServer() as connector:
        csv_to_db(connector, df, int(5e6))
        get_data_from_db(connector, most_common_complaints, 'most_common_complaints')
        get_data_from_db(connector, most_common_complaints_by_borough, 'most_common_complaints_by_borough')
        get_data_from_db(connector, most_common_complaints_agencies, 'most_common_complaints_agencies')


class SQLServer():
    """A class to run a SQL Server in Docker and close both when crash of program."""
    @logging_decoratorA
    def __enter__(self):
        system('docker run --name=mysql -p 3306:3306 --env="MYSQL_ROOT_PASSWORD=mypassword" -a stdout mysql > /dev/null &')
        
        sleep(10)
        ip = popen('docker inspect mysql | grep "\"IPAddress\""').read().split("\n")[1].split(': "')[1].strip('",')
        system('while ! wget --quiet -O /dev/null %s:3306; do sleep 1; done' % ip)
        
        connector = create_engine("mysql+pymysql://root:mypassword@" + ip + ":3306")
        conn = connector.connect()
        conn.execute("CREATE DATABASE " + DATABASE)
        self.connector = create_engine("mysql+pymysql://root:mypassword@" + ip + ":3306/" + DATABASE)
        conn = connector.connect()
        conn.execute("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));")
        return self.connector

    @logging_decoratorA
    def __exit__(self, exc_type, exc_val, exc_tb):
        sleep(1)
        system('docker container stop mysql')
        sleep(1)
        system('docker container rm mysql')


if __name__ == '__main__':
    chdir("Assignment 2")
    reset_log()
    _test3()
