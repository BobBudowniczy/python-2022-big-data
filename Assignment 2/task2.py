import sqlite3 as sq
from queries import *
from common import *
from os import chdir

DATABASE = 'requests.db'


@logging_decoratorA
def _test2(df = None):
    """Test the most common aspects of the forecast using sqlite3.

    Args:
        df ([pd.DataFrame], optional): [dataframe to work on]. Defaults to None.
    """
    if not isinstance(df, pd.DataFrame): df = load_data_csv()
    connector = create_db()
    csv_to_db(connector, df)
    get_data_from_db(connector, most_common_complaints, 'most_common_complaints')
    get_data_from_db(connector, most_common_complaints_by_borough, 'most_common_complaints_by_borough')
    get_data_from_db(connector, most_common_complaints_agencies, 'most_common_complaints_agencies')
    connector.close()


def create_db():
    """Creates a sqlite database.

    Returns:
        [connector to sqlite database]
    """
    connector = sq.connect(DATABASE)
    cursor = connector.cursor()
    query = "create table if not exists %s (" % TABLE_NAME
    for col in COLUMNS:
        query += "`" + col + "` text, "
    query = query[:-2] + ")"
    cursor.execute(query)
    return connector


if __name__ == '__main__':
    chdir("Assignment 2")
    reset_log()
    _test2()
