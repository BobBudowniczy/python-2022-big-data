most_common_complaints = """SELECT `Complaint Type`,COUNT(*) as count
                            FROM requests
                            GROUP BY `Complaint Type`
                            ORDER BY count DESC
                            LIMIT 5;"""

most_common_complaints_by_borough = """ SELECT Borough, `Complaint Type`
                                        FROM
                                        (
                                            SELECT
                                                Borough,
                                                `Complaint Type`,
                                                count,
                                                ROW_NUMBER() OVER
                                                (
                                                    PARTITION BY Borough
                                                    ORDER BY count DESC
                                                ) as ranking
                                            FROM
                                            (
                                                SELECT *, COUNT(*) as count
                                                FROM requests
                                                GROUP BY Borough, `Complaint Type`
                                            ) as tba
                                        ) as yah
                                        WHERE ranking = 1;"""

most_common_complaints_agencies = """   SELECT DISTINCT(requests.`Agency Name`), pop.`Complaint Type`
                                        FROM requests
                                        INNER JOIN
                                        (
                                            SELECT `Agency Name`, `Complaint Type`, COUNT(*) as count
                                            FROM requests
                                            GROUP BY `Complaint Type`
                                            ORDER BY count DESC
                                            LIMIT 5
                                        ) as pop ON requests.`Agency Name` = pop.`Agency Name`
                                        ORDER BY pop.`Complaint Type`;"""