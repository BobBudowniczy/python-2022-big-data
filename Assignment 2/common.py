from functools import partial, wraps
from os import system
import pandas as pd
import numpy as np
from time import time

COLUMNS = ["Complaint Type", "Borough", "Agency Name"]
DATASET_FILE = "311_Service_Requests_from_2010_to_Present.csv"
TABLE_NAME = "requests"
NROWS = None

pd.set_option('display.width', 320)
np.set_printoptions(linewidth=320)
pd.set_option('display.max_columns', 10)


def reset_log():
    """Reset log."""
    system('clear')
    with open("log.log", "w") as f: pass

def logging_decorator(method, fname: str, query: bool = False):
    """Logs a function to a log file, with time and result.

    Args:
        method (function): [function to log]
        fname (str): [name of log file]
        query (bool, optional): [additional parameter to plot if exists]. Defaults to False.

    Returns:
        function: [description]
    """
    @wraps(method)
    def wrapper(*arg, **kwargs):
        if query: print(arg[2])
        else: print(method.__name__ )
        time_start = time()
        result = method(*arg, **kwargs)
        time_stop = time()
        with open(fname, 'a') as f:
            f.write('Function: ' + method.__name__ + '\n')
            f.write(' --> Time: %.03f\n' % (time_stop - time_start))
            if query:
                f.write(' --> Args: %s\n' % str(arg[2]))
            f.write(' --> Result:\n')
            f.write(str(result) + '\n===\n')
        return result

    return wrapper


logging_decoratorA = partial(logging_decorator, fname="log.log")
logging_decoratorB = partial(logging_decorator, fname="log.log", query=True)


@logging_decoratorA
def load_data_csv() -> pd.DataFrame:
    """Loads data from DATASET_FILE.

    Returns:
        pd.DataFrame: [output as pandas DataFrame]
    """
    df = pd.read_csv(DATASET_FILE, usecols=COLUMNS, nrows=NROWS,
                     dtype={"Complaint Type": "category", "Borough": "category"})
    return df

@logging_decoratorA
def csv_to_db(connector, df: pd.DataFrame, chunksize: int | None = None):
    """Write a DataFrame to a database in the server.

    Args:
        connector: [connector to database]
        df ([pd.DataFrame]): [dataframe that has to be written to DataBase]
    """
    df.to_sql(TABLE_NAME, connector, if_exists="replace", index=False, chunksize=chunksize)


@logging_decoratorB
def get_data_from_db(connector, query: str, query_name: str) -> pd.DataFrame:
    """Get data from SQL query .

    Args:
        connector: [connector to database]
        query (str): [query to execute]
        query_name (str): [name of the query (visible in log)]

    Returns:
        [pd.DataFrame]: [table downloaded from database]
    """
    df = pd.read_sql_query(query, connector)
    return df