# data url: https://nycopendata.socrata.com/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9

from os import chdir, system
from common import load_data_csv, reset_log
from task1 import _test1
from task2 import _test2
from task3 import _test3

def log(phase: int):
    """Write the current phase to log.

    Args:
        phase (int): [number of phase]
    """
    with open("log.log", "a") as f:
        f.write("\n" + 40*"=" + "\nPhase: " + str(phase) + "\n===\n\n")

if __name__ == "__main__":
    system('rm "Assignment 2/requests.db"')
    chdir("Assignment 2")
    reset_log()
    log("Loading data to pd")
    df = load_data_csv()
    log(1)
    _test1(df)
    log(2)
    _test2(df)
    log(3)
    _test3(df)
    with open("log.log") as f:
        txt = f.read()
        print(txt)
        phases = txt.split(40*"=")[1:]
        for i, txt in enumerate(phases):
            with open("log" + str(i) + ".log", "w") as f:
                f.write(txt)
