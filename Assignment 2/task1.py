from common import *
from os import chdir


@logging_decoratorA
def _test1(df = None):
    """Test the most common aspects of the forecast using pandas.

    Args:
        df ([pd.DataFrame], optional): [dataframe to work on]. Defaults to None.
    """
    if not isinstance(df, pd.DataFrame): df = load_data_csv()
    complaints = most_common_complaints(df)
    most_common_complaints_by_borough(df)
    most_common_complaints_agencies(df, complaints.index)


@logging_decoratorA
def most_common_complaints(df: pd.DataFrame) -> pd.DataFrame:
    """Find the most common complaints in given data frame.

    Args:
        df (pd.DataFrame): [dataframe to work on]

    Returns:
        [pd.DataFrame]: [result]
    """
    complaints = df[["Complaint Type", "Agency Name"]] \
        .groupby(["Complaint Type"])[["Agency Name"]].count() \
        .sort_values(["Agency Name"], ascending=False)
    return complaints.head(5)


@logging_decoratorA
def most_common_complaints_by_borough(df: pd.DataFrame) -> pd.DataFrame:
    """Get the most common complaints by the borought.

    Args:
        df (pd.DataFrame): [dataframe to work on]

    Returns:
        [pd.DataFrame]: [result]
    """
    complaints = df[["Complaint Type", "Borough"]] \
        .groupby(["Borough"])[["Complaint Type"]].agg(pd.Series.mode)
    return complaints


@logging_decoratorA
def most_common_complaints_agencies(df: pd.DataFrame, complaints: pd.Series) -> pd.DataFrame:
    """Finds the agencies corresponding to given complaints.

    Args:
        df (pd.DataFrame): [dataframe to work on]
        complaints (pd.Series): [complaints to be listed]

    Returns:
        pd.DataFrame: [result dataframe]
    """
    agencies = df[["Agency Name", "Complaint Type"]]
    agencies = agencies[agencies["Complaint Type"].isin(complaints)].drop_duplicates()
    agencies = agencies.sort_values(["Complaint Type"])
    return agencies


if __name__ == "__main__":
    chdir("Assignment 2")
    reset_log()
    _test1()
