from psutil import virtual_memory
from os import system
from time import sleep

if __name__ == '__main__':
    mems = []
    
    while 1:
        system("clear")
        mem = virtual_memory().percent
        mems.append(mem)
    
        max_mem_10s = max(mems[-5:])
        max_mem_min = max(mems[-30:])
        max_mem_10min = max(mems[-300:])
        max_mem = max(mems)
        
        print("RAM usage:")
        print("Actual:    ", mem)
        print("Max 10s:   ", max_mem_10s)
        print("Max 1min:  ", max_mem_min)
        print("Max 10min: ", max_mem_10min)
        print("Max all:   ", max_mem)
        sleep(2)
    
