from functools import partial
from multiprocessing import Pool
from common import *
import numpy as np


def _test4(sizeA, sizeB):
    __check_sizes(sizeA, sizeB)
    matrixA = np.random.random(sizeA)
    matrixB = np.random.random(sizeB)
    result_np, time_np = task4_np(matrixA, matrixB)
    print("Result (np): ", str(result_np))
    print("Time (np)    ", time_np)
    result_simple, time_simple = task4_simple(matrixA, matrixB)
    print("Result (simple): ", str(result_simple))
    print("Time (simple)    ", time_simple)
    result_paralel, time_paralel = task4_paralel(matrixA, matrixB)
    print("Result (paralel)", str(result_paralel))
    print("Time (paralel)   ", time_paralel)
    print("==> Testing results:")
    if np.allclose(result_np, result_simple, equal_nan=True):
        print("Simple method is working.")
    if np.allclose(result_np, result_paralel, equal_nan=True):
        print("Paralel method is working.")


def __check_sizes(sizeA, sizeB):
    assert len(sizeA) == 2 and len(sizeB) == 2,\
        "Size of matrix need to be in form (n, m). You'r are %s and %s." % (sizeA, sizeB)
    assert sizeA[0] > 0 and sizeA[1] > 0 and sizeB[0] > 0 and sizeB[1] > 0,\
        "Size of matrix need to given as positive numbers."
    assert sizeA[1] == sizeB[0],\
        "Number of columns (2nd dim) of matrix A need to be same as number of rows (1st dim) of matrix B.\n"\
        "Given: A: %s; B: %s." % (sizeA, sizeB)


@time_it
def task4_np(matrixA, matrixB):
    return np.matmul(matrixA, matrixB)


@time_it
def task4_simple(matrixA, matrixB):
    sizeA = matrixA.shape
    sizeB = matrixB.shape
    result = np.zeros((sizeA[0], sizeB[1]))
    for i in range(sizeA[0]):
        result[i,:] = __mapper(matrixB, matrixA[i])
    return result


@time_it
def task4_paralel(matrixA, matrixB):
    sizeA = matrixA.shape
    sizeB = matrixB.shape
    rowsA = list(matrixA)
    with Pool() as pool:
        mapped = pool.map(partial(__mapper, matrixB), rowsA)
    result = __reducer(mapped)
    return result


def __mapper(matrixB, vectorA):
    result = np.dot(vectorA, matrixB)
    return result


def __reducer(rows):
    return np.vstack(rows)