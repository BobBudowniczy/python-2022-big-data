from os import chdir
from common import *
from math import floor
import numpy as np
from multiprocessing import Pool
from functools import reduce, partial


def _test1(N: int, M: int, minV: float, maxV: float, number_of_chunks: int):
    matrix = __random_matrix(N, M)
    expected = floor(N * M * (maxV - minV))
    print("Expected:        ", expected)
    result_simple, time_simple = task1_simple(matrix, minV, maxV)
    print("Result (simple): ", result_simple)
    print("Time (simple)    ", time_simple)
    result_paralel, time_paralel = task1_paralel(matrix, minV, maxV, number_of_chunks)
    print("Result (paralel):", result_paralel)
    print("Time (paralel)   ", time_paralel)


@time_it
def task1_simple(matrix: np.ndarray, minV, maxV) -> int:
    """A simple task that runs the task.

    Args:
        matrix (np.ndarray): matrix from which values are counted
        minV (float): lower bound for counted values
        maxV (float): upper bound for counted values

    Returns:
        int: number of numbers between min and maxV in the matrix
    """
    __check_inputV(minV, maxV)
    result = __mapper(minV, maxV, matrix)
    return result


@time_it
def task1_paralel(matrix: np.ndarray, minV: float, maxV: float, number_of_chunks: int) -> int:
    """A simple task that runs the task paralel.

    Args:
        matrix (np.ndarray): matrix from which values are counted
        minV (float): lower bound for counted values
        maxV (float): upper bound for counted values
        number_of_chunks (int): number of chunks to be used in multiprocessing

    Returns:
        int: number of numbers between min and maxV in the matrix
    """
    __check_inputV(minV, maxV)
    assert number_of_chunks > 0, "number_of_chunks must be positive (number_of_chunks=" + str(number_of_chunks) + ")"
    
    chunks = np.array_split(matrix, number_of_chunks)

    with Pool() as pool:
        mapped = pool.map(partial(__mapper, minV, maxV), chunks)
        
    result = reduce(__reducer, mapped)
    return result


def __check_inputV(minV, maxV):
    assert 0 <= minV <= 1, "minV must be betweem 0 and 1 (minV=" + str(minV) + ")"
    assert 0 <= maxV <= 1, "maxV must be betweem 0 and 1 (maxV=" + str(maxV) + ")"
    assert minV <= maxV, "minV must be <= maxV (minV=" + str(minV) + ", maxV=" + str(maxV) + ")"


def __random_matrix(N: int, M: int) -> np.ndarray:
    """Return a random matrix of size NxM."""
    assert N > 0, "N must be positive (N=" + str(N) + ")"
    assert M > 0, "M must be positive (M=" + str(M) + ")"
    
    matrix = np.random.rand(N, M)
    return matrix


def __mapper(minV: int, maxV: int, array: np.ndarray) -> int:
    """Return count of value in matrix hat lies inside boundry."""
    array = np.logical_and(array > minV, array < maxV)
    return np.sum(array)


def __reducer(p: int, c: int) -> int:
    """Sums 2 values."""
    return p + c


if __name__ == '__main__':
    chdir("Assignment 3")
    _test1()