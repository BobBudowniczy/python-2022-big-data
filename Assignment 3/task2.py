import io
from math import ceil
from os import system, chdir, listdir, popen
from typing import Dict, Iterable, List, Tuple
from common import *
from time import sleep
import zipfile
from collections import Counter
from functools import reduce
from multiprocessing import Pool
import numpy as np


START_URL = "http://www.gutenberg.org/robot/harvest?filetypes[]=txt&langs[]=en"


global iterator


def _test2(limit, number_of_chunks = 30):
    global iterator
    download_library(limit)
    chunks, file_list = __prepare_chunks(limit, number_of_chunks)
    print("Number of books to proceed:", len(file_list))
    iterator = 0
    result_simple, time_simple = task2_simple(chunks)
    print("\rResult (simple): ", str(result_simple)[:200])
    print("Time (simple)    ", time_simple)
    iterator = 0
    result_paralel, time_paralel = task2_paralel(chunks)
    print("\rResult (paralel):", str(result_paralel)[:200], 2*len(file_list)*" ")
    print("Time (paralel)   ", time_paralel)


def download_library(limit):
    """Downloads books from START_URL. Download limitet to depth of PAGES_OF_ZIPS.
    Downloaded books are placed in "books/" folder.
    
    If file is downloaded, it will not be downloaded again."""
    pages_to_download = ceil(limit/55)
    system("wget -r -l " + str(pages_to_download) + " -H --waitretry 2 -nd -P books/ -nc -R *-8.zip -o wget_log.log " + START_URL)
    print("Starting/Continuing downloading of books.", end="\n\n")
    
    __download_library_waiter(limit)
    
    system('rm books/*txt')
    print("\rBooks downloading completed.")


def __download_library_waiter(limit):
    """Check for number of files in "book" folder to stop changing."""
    old_num_of_files = len(listdir("books"))
    sleep(5)
    while 1:
        sleep(5)
        num_of_files = len(listdir("books"))
        num_of_books = popen('ls books/*.zip | wc -l').read()
        print("\033[A\r", "Number of books downloaded:", num_of_books, end="")
        if num_of_files == old_num_of_files or int(num_of_books) >= limit: break
        old_num_of_files = num_of_files


Chunks = List[Iterable[str]]


def __prepare_chunks(limit: int, number_of_chunks: int) -> Tuple[Chunks, List[str]]:
    """Divide given list into chunks and sort it."""
    print("Preparing chunks.")
    file_list = listdir("books")[:limit]
    file_list.sort()
    chunks = np.array_split(file_list, number_of_chunks)
    return chunks, file_list


@time_it
def task2_simple(chunks: Chunks) -> Dict[str, int]:
    """Returns counts of words in zip files with names inside of chunks.
    Use one-core."""
    print("==> Simple method running")
    mapped = map(__chunk_mapper, chunks)
    result = reduce(__reducer, mapped, Counter())
    return result


@time_it
def task2_paralel(chunks: Chunks) -> Counter:
    """Returns counts of words in zip files with names inside of chunks.
    Use multiple cores."""
    print("==> Paralel method running")
    with Pool(4) as pool:
        mapped = pool.map(__chunk_mapper, chunks)
    result = reduce(__reducer, mapped, Counter())
    return result


def __chunk_mapper(file_list: List[str]) -> Counter:
    """Returns counts of words in zip files with names inside of file_list."""
    mapped = map(__mapper, file_list)
    result = reduce(__reducer, mapped, Counter())
    return result


def __mapper(file: str) -> Counter:
    """Returns counts of words in zip file with name given as file."""
    global iterator
    fname = file.rstrip('.zip') + '.txt'
    with zipfile.ZipFile("books/"+file, 'r') as archive:
        with io.TextIOWrapper(archive.open(fname), errors="ignore") as book:
            book_txt = book.read()
            slicer = book_txt.find(6 * "\n")
            if slicer < -1:
                slicer = book_txt.find(5 * "\n")
            book_txt = book_txt[slicer:]
    words = book_txt.split()
    iterator += 1
    if iterator % 10 == 0:
        print("\r", "Done for", iterator, "files.", end="")
    return Counter(words)


def __reducer(p: Counter, c: Counter) -> Counter:
    """Sums 2 Counters."""
    return p+c


if __name__ == "__main__":
    chdir("Assignment 3")
    _test2()
