from decorator import decorator
from time import time


@decorator
def time_it(func, *args, **kwargs):
    """Decorator to time execution of a function.

    Args:
        func: function to measure it time
        *args, **kwargs: args and kwargs of a function

    Returns:
        result: result of a function
        exec_time: time of function execution in seconds
    """
    start = time()
    result = func(*args, **kwargs)
    stop = time()
    exec_time = stop - start
    return result, exec_time
