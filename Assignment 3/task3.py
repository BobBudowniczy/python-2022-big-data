from os import chdir
from common import *
from math import pi
from multiprocessing import Pool


def _test3(limit, number_of_chunks):
    print("Expected", pi)
    result_simple, time_simple = task3_simple(limit)
    print("Result (simple): ", str(result_simple)[:200])
    print("Time (simple)    ", time_simple)
    result_paralel, time_paralel = task3_paralel_simple(limit)
    print("Result (paralel, simple)", str(result_paralel)[:200])
    print("Time (paralel, simple)   ", time_paralel)
    result_paralel, time_paralel = task3_paralel(limit, number_of_chunks)
    print("Result (paralel)", str(result_paralel)[:200])
    print("Time (paralel)   ", time_paralel)


@time_it
def task3_simple(limit):
    ks = range(limit)
    vals = map(__mapper, ks)
    return sum(vals)


@time_it
def task3_paralel_simple(limit):
    ks = range(limit)
    with Pool() as pool:
        vals = pool.map(__mapper, ks)
    return sum(vals)


@time_it
def task3_paralel(limit, number_of_chunks):
    def chunks():
        for i in range(number_of_chunks):
            yield range(i, limit, number_of_chunks)
    
    with Pool() as pool:
        vals = pool.map(__chunk_mapper, chunks())
    return sum(vals)


def __chunk_mapper(file_list):
    """Returns counts of words in zip files with names inside of file_list."""
    mapped = map(__mapper, file_list)
    result = __reducer(mapped)
    return result


def __mapper(k):
    frac = lambda a, b, c: a / (b*k + c)
    params = [[4, 8, 1], [-2, 8, 4], [-1, 8, 5], [-1, 8, 6]]
    inside_bracket = sum(map(lambda x: frac(*x), params))
    return 1 / 16**k * inside_bracket


def __reducer(lista):
    return sum(lista)


if __name__ == "__main__":
    chdir("Assignment 3")
    _test3()

