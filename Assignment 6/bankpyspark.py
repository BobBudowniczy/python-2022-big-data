import findspark
import pandas as pd
from decorator import decorator
from pyspark import SparkContext, SparkConf
from pyspark.ml import Pipeline
from pyspark.sql import SparkSession, DataFrame
from pyspark.ml.feature import OneHotEncoder, VectorAssembler, StringIndexer
from pyspark.ml.classification import LogisticRegression, DecisionTreeClassifier, RandomForestClassifier, GBTClassifier
from pyspark.ml.evaluation import BinaryClassificationEvaluator


class PySparkBankSession:
    categorical_features = ["job", "marital", "education", "default", "housing", "loan", "contact", "poutcome"]
    numerical_features = ["balance", "duration", "campaign", "pdays", "previous"]
    label_col = "deposit"
    ml_model_classes = [LogisticRegression, DecisionTreeClassifier, RandomForestClassifier, GBTClassifier]
    sc: SparkContext
    spark: SparkSession
    df: DataFrame

    @decorator
    def log_decorator(fun, text: str = "", *args, **kwargs):
        """Decorator for printing done steps."""
        print(f"{text}", end='')
        result = fun(*args, **kwargs)
        print(f"\r{text} - DONE")
        return result

    @log_decorator(text="Initializing PySpark session.")
    def __init__(self):
        """Initialize method for PySparkBankSession."""
        findspark.init()
        self.sc = SparkContext(conf=SparkConf())
        self.sc.setLogLevel("ERROR")
        self.spark = SparkSession(sparkContext=self.sc)
        self.df = self.read_csv("bank.csv")
        self.train, self.test = None, None
        self.classification_results = None

    @log_decorator(text="Reading csv file.")
    def read_csv(self, filename: str) -> DataFrame:
        """Loads csv to pyspark."""
        df = self.spark.read.csv(filename, header=True, inferSchema=True)
        return df

    @log_decorator(text="Removing unnecessary columns.")
    def clear_dataset(self):
        """Removes unused features from the dataset."""
        self.df = self.df.select(self.categorical_features + self.numerical_features + [self.label_col])

    @log_decorator(text="Preparing features.")
    def get_features(self):
        """Create features column in dataframe."""
        categorical_features_index = list(map(lambda x: x + "_index", self.categorical_features))
        categorical_features_onehot = list(map(lambda x: x + "_OneHot", self.categorical_features))
        features = categorical_features_onehot + self.numerical_features
        # pipeline steps
        indexer_step = StringIndexer(inputCols=self.categorical_features, outputCols=categorical_features_index)
        indexer_step_label = StringIndexer(inputCol=self.label_col, outputCol="label")
        onehot_step = OneHotEncoder(inputCols=categorical_features_index, outputCols=categorical_features_onehot)
        assembler_step = VectorAssembler(inputCols=features, outputCol="features")
        # running things up
        pipeline = Pipeline(stages=[indexer_step, indexer_step_label, onehot_step, assembler_step])
        model = pipeline.fit(self.df)
        df = model.transform(self.df)
        self.df = df.select(["features", "label", self.label_col]
                            + self.categorical_features + self.numerical_features)

    @log_decorator(text="Splitting to training and test set.")
    def train_test_split(self, part=0.2):
        """Splits dataset to train and test."""
        self.train, self.test = self.df.randomSplit([1 - part, part])

    @log_decorator(text="Making a classification in a couple of methods.\n")
    def do_classification(self):
        """Makes classification in a couple of methods.
        List of methods is available in ml_model_classes class parameter."""
        results = {}
        for model in self.ml_model_classes:
            classifier = model(featuresCol="features", labelCol="label")
            model = classifier.fit(self.train)
            predictions = model.transform(self.test)
            evaluator = BinaryClassificationEvaluator()
            result = evaluator.evaluate(predictions)
            results[classifier.__class__.__name__] = result
            print(" > " + classifier.__class__.__name__ + " - DONE")
        self.classification_results = results

    def prepare_classification_results(self):
        df = pd.DataFrame.from_dict(self.classification_results, orient='index', columns=["score"])
        return df

    def close(self):
        """Close pyspark session."""
        self.sc.stop()
        print("Spark session is closed.")
        del self
