from bankpyspark import PySparkBankSession


def main():
    """Displays steps of task 1."""
    message = "Press ENTER to continue."
    session = PySparkBankSession()
    session.df.show()
    input(message)
    session.clear_dataset()
    session.df.show()
    input(message)
    session.get_features()
    session.df.show()
    input(message)
    session.train_test_split(part=0.2)
    session.do_classification()
    results = session.prepare_classification_results()
    print(results)
    input(message)
    session.close()


if __name__ == "__main__":
    main()
